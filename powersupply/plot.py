import csv
import cairo
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject, Gio, GdkPixbuf

def plot(ctx, data, y_col, color, x_factor, y_factor, shift_y):
        x_col = 0;
        ctx.set_source_rgba(*color)
        ctx.move_to(data[0][x_col] * x_factor, data[0][y_col] * y_factor + shift_y)
        for point in data[1:]:
            x = point[x_col] * x_factor
            y = point[y_col] * y_factor + shift_y
            ctx.line_to(x, y)
        ctx.set_dash([])
        ctx.stroke()

def plot_dashes(ctx, data, y_col, modifier_col, modifier_invert, color, x_factor, y_factor, shift_y):
        x_col = 0;
        for point in data:
            x = point[x_col] * x_factor
            y = point[y_col] * y_factor + shift_y
            modifier = point[modifier_col]
            if modifier_invert:
                modifier = not modifier
            if modifier:
                ctx.move_to(x, 0)
                ctx.line_to(x, y)
                gradient = cairo.LinearGradient(x, y, x, 0)
                gradient.add_color_stop_rgba(0, (*color), 0.5)
                gradient.add_color_stop_rgba(1, (*color), 0)
                ctx.set_source(gradient)
                ctx.stroke()

def plot_hour_marks(ctx, data, height, color, x_factor):
        x_col = 0
        ctx.set_source_rgba(*color)
        for i in range(0, int(data[-1][x_col])):
            if i % (60*60) == 0:
                ctx.move_to(i * x_factor, 0)
                ctx.line_to(i * x_factor, height)
        ctx.set_dash([])
        ctx.stroke()

def plot_percentage_marks(ctx, data, width, color, y_factor):
        x_col = 0
        ctx.set_source_rgba(*color)
        for i in range(0, 100):
            if i % 10 == 0:
                ctx.move_to(0, i * y_factor)
                ctx.line_to(width, i * y_factor)
        ctx.set_dash([])
        ctx.stroke()

def draw(ctx, widget, graph_data, length, advanced):
    colors = [
            (0.26, 0.86, 0.26),
            (1, 0, 0),
            (0, 0.5, 1),
            (0.5, 0.33, 0),
        ]

    style = widget.get_style_context()
    state = Gtk.StateFlags(1)
    font = style.get_font(state)
    font_color = style.get_color(state)

    ctx.set_font_size(font.get_size() / 1024)
    ctx.select_font_face(font.get_family(), cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)

    size = widget.get_allocation()

    width = size.width
    height = size.height

    ctx.transform(cairo.Matrix(yy=-1, y0=height));

    ctx.set_line_width(2)

    x_factor = width / (150*length)

    plot_hour_marks(ctx, graph_data, 10, (1, 1, 1), x_factor)

    if advanced:
        plot(ctx, graph_data, 4, colors[3], x_factor, 50, centerline+1)
        plot(ctx, graph_data, 1, colors[0], x_factor, height/100, 0)
        plot(ctx, graph_data, 2, colors[1], x_factor, height/800, 0)
        plot(ctx, graph_data, 3, colors[2], x_factor, height/15, 0)
    else:
        plot_dashes(ctx, graph_data, 1, 4, 0, (0.15, 0.45, 0.15), x_factor, height/100, 0)
        plot_dashes(ctx, graph_data, 1, 4, 1, (0.45, 0.15, 0.15), x_factor, height/100, 0)
        plot_percentage_marks(ctx, graph_data, 10, (1, 1, 1), height/100)
        plot(ctx, graph_data, 1, colors[0], x_factor, height/100, 0)
